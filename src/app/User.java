package app;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;




public class User {
	
		public String username;
		public ArrayList<Album> albums;
		public User (String username, ArrayList<Album> albums) {
			this.username = username;
			this.albums = albums;
		}
		
		public class Album {
			public String name;
			public ArrayList<Photo> photos;
			public Album (String name, ArrayList<Photo> photos) {
					this.name = name;
					this.photos = photos; 
				}
				
				public class Photo {
					public Date date;
					public String caption;
					public String file_system_location;
					public HashMap<String,String> tags;
					
					public Photo (Date date, String caption, String file_system_location, HashMap<String,String> tags) {
						this.date = date;
						this.caption = caption;
						this.file_system_location = file_system_location;
						this.tags = tags;
					}
				}
				
			}

	
}
