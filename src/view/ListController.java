package view;

 
import java.util.ArrayList;
import java.util.Optional;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;

import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Callback;


import java.io.FileNotFoundException;
import java.io.IOException;



import app.User;
import javafx.scene.Node;
import java.io.FileInputStream;

import java.util.Calendar;

import java.util.Date;
import java.util.HashMap;

public class ListController {
	
	
    public static ArrayList<User> users;
    
    public static Calendar cal = Calendar.getInstance();
    
    public static User currentUser;
    
    public static User.Album currentAlbum;
    
    public static User.Album.Photo currentPhoto;
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	
	
 // login page ---------------------------------
	@FXML
    private Button login_button;

    @FXML
    private TextField login_textfield;
    
   
    
    @FXML
    void login(ActionEvent event) {
    	/*String input = login_textfield.getText();
    	int user_index = getUserIndex(input); 
    	  if (input.equals("admin")) {
    		  try {
        		  switchScenetoAdminInitPage(event);
        	  }
        	  catch (IOException e){
        		  e.printStackTrace();
        	  }
    	  }
    	  else {
    		 currentUser = users.get(user_index);
    		 try {
    			 switchScenetoUserInitPage(event);
       	  	 }
       	  	catch (IOException e){
       		  e.printStackTrace();
       	  	}
    	  }*/
		
    	try {
			 switchScenetoUserInitPage(event);
 	  	 }
 	  	catch (IOException e){
 		  e.printStackTrace();
 	  	} 
		
    	  
  	    
    	
    }
    
    private int getUserIndex(String username) {
    	for (int i = 0; i < users.size(); i++) {
    		if (users.get(i).username.equals(username)) {
    			return i;
    		}
    	}
    	return -1;
    }
    
    public void switchScenetoAdminInitPage(ActionEvent event) throws IOException {
    	  root = FXMLLoader.load(getClass().getResource("admininitpage.fxml"));
    	  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
    	  scene = new Scene(root);
    	  stage.setScene(scene);
    	  stage.show();
    }
    
    public void switchScenetoUserInitPage(ActionEvent event) throws IOException {
   	  root = FXMLLoader.load(getClass().getResource("userinitpage.fxml"));
   	  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
   	  scene = new Scene(root);
   	  stage.setScene(scene);
   	  stage.show();
   }
    
   /* public boolean containsUser(String username) {
    	try {
    		File users_file = new File ("src/app/users.txt");
        	Scanner file_scanner = new Scanner(users_file);
        	while (file_scanner.hasNextLine()) {
        		if (file_scanner.nextLine().equals(username)) {
        			return true;
        		}
        	}
        	file_scanner.close();
        	return false;
    	}
    	catch (FileNotFoundException e) {
    		e.printStackTrace();
    		return false;
    	}
    }*/
    
    //admin initial page---------------------------------
    
    @FXML
    private Button addusers_button;

    @FXML
    private Button deleteusers_button;

    @FXML
    private Button listusers_button;

    @FXML
    private Button logout_button_admin;

    @FXML
    private ListView<String> users_listview;
    
    private ObservableList<String> username_list;
    

    @FXML
    void addUsers(ActionEvent event) {
    	/*TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add User");
		dialog.setHeaderText("Enter the name of user to add");
		dialog.setContentText("Enter username: ");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			username_list.add(response);
			
		}*/
		
		TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Album");
		dialog.setHeaderText("Enter the name of album to add");
		dialog.setContentText("Enter name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			if ((username_list.contains(response))) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("user already exists");
	    		error_mess.show();
			}
			else {
				username_list.add(response);
			    users.add(new User(response, new ArrayList<User.Album>()));
			}
		}
    	/*try {	
    		File users_file = new File ("src/app/users.txt");
        	Scanner file_scanner = new Scanner(users_file);
        	String old_content = ""; 
        	while (file_scanner.hasNextLine()) {
        		old_content += (file_scanner.nextLine() + "\n");
        	}
        	file_scanner.close();
    	      FileWriter myWriter = new FileWriter("src/app/users.txt");
    	      myWriter.write(old_content);
    	      myWriter.close();
    	    } catch (IOException e) {
    	      e.printStackTrace();
    	    }*/
    	   
          }
    
    @FXML
    void listUsers(ActionEvent event) {
    	users_listview.setItems(username_list);
    	/*try {
    		File users_file = new File ("src/app/users.txt");
        	Scanner file_scanner = new Scanner(users_file);
        	while (file_scanner.hasNextLine()) {
        		String user = file_scanner.nextLine();
        		username_list.addAll(user);
        	}
        	users_listview.setItems(username_list); 
        	file_scanner.close();
    	}
    	catch (FileNotFoundException e) {
    		e.printStackTrace();
    	}*/
    	
    	
    }
    
    @FXML
    void deleteUsers(ActionEvent event) {
    	/*TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Delete User");
		dialog.setHeaderText("Enter the name of user to delete");
		dialog.setContentText("Enter name: ");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			username_list.remove(response);
			int user_index = getUserIndex(response);
			users.remove(user_index);
			
		}*/
    	/*TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Delete User");
		dialog.setHeaderText("Enter the name of user to remove");
		dialog.setContentText("Enter username: ");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			username_list.remove(response);
			
		}*/
		
		TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Album");
		dialog.setHeaderText("Enter the name of album to add");
		dialog.setContentText("Enter name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			if (!(username_list.contains(response))) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("user does not exist");
	    		error_mess.show();
			}
			else {
				album_list.remove(response);
				int user_index = getUserIndex(response);
			    users.remove(user_index);
			}
		}
    }
    
    @FXML
    void logout(ActionEvent event) {
    	//System.exit(0);
    }
    
    //user initial page---------------------------------
    
    @FXML
    private Button addalbum_button;
    
    @FXML
    private Button listalbums_button;
    

    @FXML
    private ListView<String> album_listview;
    
    
    private ObservableList<String> album_list;

    @FXML
    private TextField daterange_textfield;

    @FXML
    private Button deletealbum_button;

    @FXML
    private Button logout_button_user;

    @FXML
    private Button renamealbum_button;

    @FXML
    private Button searchandmakealbum_button;

    @FXML
    private Button searchphotos_button;

    @FXML
    private TextField tags_textfield;
    
    @FXML
    void listAlbums(ActionEvent event) {

    }

    @FXML
    void addAlbum(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Album");
		dialog.setHeaderText("Enter the name of album to add");
		dialog.setContentText("Enter album name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			if (album_list.contains(response)) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("Album already exists");
	    		error_mess.show();
			}
			else {
				album_list.add(response);
			    currentUser.albums.add(currentUser.new Album(response, new ArrayList<User.Album.Photo>()));
			}
		}
		
		
    }

    @FXML
    void deleteAlbum(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Delete Album");
		dialog.setHeaderText("Enter the name of album to delete");
		dialog.setContentText("Enter album name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			if (!(album_list.contains(response))) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("Album doesn't exist");
	    		error_mess.show();
			}
			else {
				album_list.remove(response);
				currentUser.albums.remove(getAlbumIndex(response));
			}
		}
    }
    
    private int getAlbumIndex(String album_name) {
    	ArrayList<User.Album> albums = currentUser.albums;
    	for (int i = 0; i < albums.size(); i++) {
    		if (albums.get(i).name.equals(album_name)) {
    			return i;
    		}
    	}
    	return -1;
    }
    
   /* private int getAlbumIndex (User user, String album_name) {
    	for (int i = 0; i < user.albums.size(); i++) {
    		if (user.albums.get(i).name.equals(album_name)) {
    			return i;
    		}
    	}
    	return -1;
    }*/
    
    @FXML
    void renameAlbum(ActionEvent event) {
    	try {
    		switchtoOpenAlbumPage(event);
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    	
    	
    	/*TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Rename Album");
		dialog.setHeaderText("Enter the name of album to rename");
		dialog.setContentText("Enter album name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			if (!(album_list.contains(response))) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("Album doesn't exist");
	    		error_mess.show();
			}
			else {
				TextInputDialog dialog2 = new TextInputDialog();
			    dialog2.setTitle("Rename Album");
				dialog2.setHeaderText("Enter the new name of the album");
				dialog2.setContentText("Enter album name: ");
				
				Optional<String> result2 = dialog2.showAndWait();
				if (result2.isPresent()) {  
					String response2 = result2.get();
					ArrayList<User.Album> album_list = currentUser.albums;
			        for (int i = 0; i < album_list.size(); i++) {
			        	if (album_list.get(i).name.equals(response)) {
			        		album_list.set(i,currentUser.new Album(response2, new ArrayList<User.Album.Photo>()));
			        	}
			        }
				
				}
			}
		}*/
    	
    	
    }

    @FXML
    void searchAndmakeAlbum(ActionEvent event) {
    	
    }

    @FXML
    void searchPhotos(ActionEvent event) {
    	

    }
    
    public void switchtoOpenAlbumPage(ActionEvent event) throws IOException {
    	  root = FXMLLoader.load(getClass().getResource("openalbum.fxml"));
    	  stage = (Stage)((Node)event.getSource()).getScene().getWindow();
     	  scene = new Scene(root);
     	  stage.setScene(scene);
     	  stage.show();
     	  
    	 
     	 
     	 
     	/* ArrayList<User.Album.Photo> photos = new ArrayList<User.Album.Photo>();
     	 for (int i = 0; i < photos.size(); i++) {
     		 photo_list_data.add(photos.get(i).caption);
     	 }*/
         
     	  
     	  //  ObservableList<User.Album.Photo> photos = users.get(0).albums.get(0).photos;
     	 /* for (int i = 0; i < photos.size();i++) {
     		  photo_list_data.add(photos.get(i).caption);
     	  }
     	  photo_list.setItems(photo_list_data);
     	  photo_list.setCellFactory(param -> new ListCell<String>() {
             private ImageView imageView = new ImageView();
             @Override
             public void updateItem(String name, boolean empty) {
                 super.updateItem(name, empty);
                 if (empty) {
                     setText(null);
                     setGraphic(null);
                 } else {
                	 imageView.setImage(new Image("data/orangeflower.jpeg"));
                     setText(name);
                     setGraphic(imageView);
                 }
             }
         });*/
     	 
    
         
    }
    
   

    
    
  //open album page--------------------------------- 
   
    
    @FXML
    private Button show_photosbutton;
    
    @FXML
    private Button add_photo_button;

    @FXML
    private Button add_tag;

    @FXML
    private Button back_button;

    @FXML
    private Button caption_photo_button;

    @FXML
    private Button copy_photo_from_album_button;

    @FXML
    private Button delete_photo_button;

    @FXML
    private Button delete_tag_button;

    @FXML
    private Button display_photo_button;

    @FXML
    private Button move_photo_from_album;

    @FXML
    private Button next_photo_button;

    @FXML
    private ListView<String> photo_list;
    
    public static ObservableList<String> photo_list_data;
    

    @FXML
    private ImageView photo_view;
    

    @FXML
    private Button prev_photo_button;
    
    
    @FXML
    void showPhotos(ActionEvent event) {
    	photo_list_data = FXCollections.observableArrayList();
    	ArrayList<User.Album.Photo> photos = currentAlbum.photos;
    	for (int i = 0; i < photos.size(); i++) {
    		photo_list_data.add(photos.get(i).file_system_location);
    	}
    	photo_list.setItems(photo_list_data);
        photo_list.setCellFactory(new Callback<ListView<String>, 
                ListCell<String>>() {
                    @Override 
                    public ListCell<String> call(ListView<String> list) {
                        return new ImageCell();
                    }
                }
            );
        photo_list.getSelectionModel().select(0);
        if (!(currentAlbum.photos.isEmpty())) {
        	try {
        		photo_view.setImage(new Image(new FileInputStream(currentAlbum.photos.get(0).file_system_location)));
        		currentPhoto = currentAlbum.photos.get(0);
        	}
        	catch (FileNotFoundException e) {
        		e.printStackTrace();
        	}
        }
        
       
    }
    
    
    static class ImageCell extends ListCell<String> {
        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            ImageView image = new ImageView();
            if (item != null) {
                try {
                	image.setFitWidth(25);
                	image.setFitHeight(25);
                	image.setImage(new Image(new FileInputStream(item)));
                    setText(getCaption(item));
                    setGraphic(image);
                }
                catch (FileNotFoundException e) {
                	e.printStackTrace();
                }
                
               
            }   
        }
    }
    
    public static String getCaption (String location) {
    	ArrayList<User.Album.Photo> photos = currentAlbum.photos;
    	for (int i = 0 ; i < photos.size(); i++) {
    		if (photos.get(i).file_system_location.equals(location)) {
    			return photos.get(i).caption;
    		}
    	}
    	return null;
    }
    
    @FXML
    void addPhoto(ActionEvent event) {
    	Date date = cal.getTime();
    	
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Photo");
		dialog.setHeaderText("Enter the location of the photo (in file system) to add");
		dialog.setContentText("Enter photo location: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String location = result.get();
			currentAlbum.photos.add(currentAlbum.new Photo(date,"",location,new HashMap<String,String>()));
			
		}
    }

    @FXML
    void addTag(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Tag(s)");
		dialog.setHeaderText("Enter the tag to add to photo in the form: t1=v1");
		dialog.setContentText("Enter tag: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			
			String[] tag_response = response.split("=");
			if (currentPhoto.tags.containsKey(tag_response[0])) {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("Tag already exists");
	    		error_mess.show();
			}
			else {
				currentPhoto.tags.put(tag_response[0],tag_response[1]);
			}
		}
    }

    @FXML
    void captionPhoto(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Add Caption");
		dialog.setHeaderText("Enter the caption to add to selected photo");
		dialog.setContentText("Enter album name: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			int index = photo_list.getSelectionModel().getSelectedIndex();
			photo_list_data.set(index,response);
			currentPhoto.caption = response;
		}
    }

    @FXML
    void copyPhotofromAlbum(ActionEvent event) {
    	
    }

    @FXML
    void deletePhoto(ActionEvent event) {
    	int index = photo_list.getSelectionModel().getSelectedIndex();
    	currentAlbum.photos.remove(index);
    	
    }

    @FXML
    void deleteTag(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog();
	    dialog.setTitle("Delete Tag(s)");
		dialog.setHeaderText("Enter the tag to delete from photo in the form: t1=v1");
		dialog.setContentText("Enter tag: ");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {  
			String response = result.get();
			
			String[] tag_response = response.split("=");
			if (currentPhoto.tags.containsKey(tag_response[0])) {
				currentPhoto.tags.remove(tag_response[0]);
			}
			else {
				Alert error_mess = new Alert(AlertType.ERROR);
	    		error_mess.setContentText("Tag does not exist");
	    		error_mess.show();
			}
		}
    }

    @FXML
    void displayPhoto(ActionEvent event)  {
    	try {
    		switchtoDisplayPhoto(event);
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    	
    	
    }
    
    private void switchtoDisplayPhoto(ActionEvent event) throws IOException {
    	  root = FXMLLoader.load(getClass().getResource("displayphoto.fxml"));
 	      stage = (Stage)((Node)event.getSource()).getScene().getWindow();
 	      scene = new Scene(root);
 	      stage.setScene(scene);
 	      stage.show();
    }

    @FXML
    void goBack(ActionEvent event) {
    	try {
    		switchScenetoUserInitPage(event);
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void movePhotofromAlbum(ActionEvent event) {

    }

    @FXML
    void nextPhoto(ActionEvent event) {
    	
    	
    	/*try {
    		
    		photo_image = new Image(new FileInputStream("src/data/rose.png"));
    		photo_view.setImage(photo_image);
    	}
    	catch (FileNotFoundException e) {
    		e.printStackTrace();
    	}*/
    	
    }
    
    
    
    
    @FXML
    void prevPhoto(ActionEvent event) {
    	
    	
    	
    	
    }
    
  //display album picture---------------------------------  
    
    @FXML
    private Button showphoto_button;
    
    @FXML
    private Button back_button_album;

    @FXML
    private TextArea caption_date_photo_textfield;

    @FXML
    private ImageView photo_view_display;
    	
    @FXML
    private TextArea tags_text_field;
    
    @FXML
    void DisplayPhoto(ActionEvent event) {
    	try {
    		photo_view_display.setImage(new Image(new FileInputStream(currentPhoto.file_system_location)));
    	}
    	catch (FileNotFoundException e) {
    		e.printStackTrace();
    	}
    	caption_date_photo_textfield.setText(currentPhoto.caption);
    	String tags_output = "";
    	for (String key: currentPhoto.tags.keySet()){  
			tags_output += (key + " = " + currentPhoto.tags.get(key) + "\n");
		}
    	tags_text_field.setText(tags_output);
    }

    @FXML
    void goBacktoAlbum(ActionEvent event) {
    	try {
    		switchtoOpenAlbumPage(event);
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    }
    
	/*@FXML         
	ListView<String> listView;            */    

	//private ObservableList<String> obsList;              

	public void start(Stage mainStage) {              
		cal.set(Calendar.MILLISECOND,0);
		
		// create an ObservableList 
		// from an ArrayList  
		/*obsList = FXCollections.observableArrayList(                               
				"Rams",                               
				"Bengals",
				"49ers",
				"Giants",
				"Packers",
				"Colts",
				"Cowboys",
				"Broncos",
				"Vikings",
				"Dolphins",
				"Titans",
				"Seahawks",
				"Steelers",
				"Jaguars"); 

		listView.setItems(obsList); 

		// select the first item
		listView.getSelectionModel().select(0);

		// set listener for the items
		listView
		.getSelectionModel()
		.selectedIndexProperty()
		.addListener(
				(obs, oldVal, newVal) -> 
				//showItem(mainStage)
				showItemInputDialog(mainStage)
				); */

	}
	
	//private void showItem(Stage mainStage) {                
		/*Alert alert = new Alert(AlertType.INFORMATION);
		alert.initOwner(mainStage);
		alert.setTitle("List Item");
		alert.setHeaderText(
				"Selected list item properties");

		String content = "Index: " + 
				listView.getSelectionModel()
		.getSelectedIndex() + 
		"\nValue: " + 
		listView.getSelectionModel()
		.getSelectedItem();

		alert.setContentText(content);
		alert.showAndWait();
	}
	
	private void showItemInputDialog(Stage mainStage) {                
		String item = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();

		TextInputDialog dialog = new TextInputDialog(item);
		dialog.initOwner(mainStage); dialog.setTitle("List Item");
		dialog.setHeaderText("Selected Item (Index: " + index + ")");
		dialog.setContentText("Enter name: ");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) { obsList.set(index, result.get()); }
	*/ //}

}
